const registerForm = document.forms.register;
console.log(registerForm);
const url = "http://localhost:1337";

registerForm.addEventListener('submit', register);

const loginForm = document.forms.login;
loginForm.addEventListener('submit', login);


function register(e) {
    e.preventDefault();
    const username = registerForm.username.value;
    const email = registerForm.email.value;
    const password = registerForm.password.value;
    console.log(username, email, password);
    const payload = {
        username: username,
        email: email,
        password: password,

    };

    fetch(`${url}/auth/local/register`, {
        method: 'POST',
        headers: {
            "Content-Type": "application/json"
        },
        body: JSON.stringify(payload),
    })
        .then((res) => res.json())
        .then((data) => {
            console.log(data);
        })
        .catch((err) => {
            console.log(err);
        });

}

function login(e) {
    e.preventDefault();
    const loginemail = loginForm.loginemail.value;
    const loginpassword = loginForm.loginpassword.value;
    console.log(loginemail, loginpassword);

    const payload = {
        identifier: loginemail,
        password: loginpassword,
    };

    fetch(`${url}/auth/local`, {
        method: "POST",
        headers: {
            "Content-Type": "application/json",
        },
        body: JSON.stringify(payload),
    })
        .then((res) => res.json())
        .then((data) => {
            console.log(data);
            localStorage.setItem("user", JSON.stringify(data));
        })
        .catch((err) => {
            console.error(err);
        });
}
